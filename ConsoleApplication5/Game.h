#ifndef GAME_H
#define GAME_H
#include "Board.h"
#include "Player.h"
class Game
{private:
	Board * currentBoard;
	int round;
	int nPLayer;
	int selcard;//compteur pour le round
	Player* listPl[4];
	const Card* current;
	const Card* previous;
public:
	Game(Board* e):selcard(0),round(1),currentBoard(e), nPLayer(0){
		
	}
	int getRound() const{
		return round;
	}
	void setRound() {
	  round+=1;
	}
	void addPlayer(const  Player& ent) {
		listPl[nPLayer] = new Player(ent.getName(),static_cast<Side>(nPLayer));
		nPLayer++;
	}
	Player& getPlayer(Side e) const {
		Player& squi = *(listPl[static_cast<int>(e)]);
		return squi;
	}
	Player& getPlayer(Side e) {
		return *listPl[static_cast<int>(e)];
	}
	
	const Card* getPreviousCard() const {
		return previous;
	}
	const Card* getCurrentCard() const {
		return current;
	}
	void setCurrentCard(const Card* next) {
		previous = current;
		current = next;
	}
	Card* getCard(const Letter& let , const Number& num) {
		return currentBoard->getCard(let, num);
	}
	void setCard(const Letter& let, const Number& num, Card* car) {
		currentBoard->setCard(let, num, car);
	}
	friend ostream& operator<<(ostream& os, Game& dt);
};
#endif
