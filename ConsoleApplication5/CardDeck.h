#include "Card.h"
#include "Deck.h"
#ifndef CARDDECK_H
#define CARDDECK_H
#include <random>

class CardDeck :public Deck<Card>
{
protected:
	virtual std::string Name() {
		return "CardDeck";
	}
private:
	CardDeck():Deck<Card>(50) {
		//Cree avec une combinaison aleatoire de couleur animaux le deck de card
		for (int i = 0; i < size; i++) {
				std::random_device rng;
				uniform_int_distribution<int> gen(0, 4);
				uniform_int_distribution<int> gen2(5, 9);
				vectorDeck.push_back(new Card(static_cast<FaceAnimal>(gen2(rng)), static_cast<FaceBackground>(gen(rng))));		
			
		} 
	}
public:
	void operator=(CardDeck const &x) = delete;// Empeche la classe d'etre assigner
	CardDeck(CardDeck const &) = delete;//empeche la classe d'etre copier
	static CardDeck& make_CardDeck();
	
};

#endif