#include "Rule.h"





const Player & Rule::getNextPlayer(const Game &current )
{
	if (currentPlayer == numPlayer-1) {
		currentPlayer = 0;
	}
	else { currentPlayer+=1; }
	while (!current.getPlayer(static_cast<Side>(currentPlayer)).isActive()) {
		if (currentPlayer == numPlayer - 1) {
			currentPlayer = 0;
		}
		else { currentPlayer += 1; }
	}
	return current.getPlayer(static_cast<Side>(currentPlayer));
}

Rule::~Rule()
{
}
