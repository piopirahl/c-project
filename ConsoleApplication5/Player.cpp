#include "Player.h"


Side Player::getSide() const{
	return side;
}

void Player::setSide(Side tside) {
	side = tside;
}


ostream& operator<<(ostream& os,  Player& dt)
{
	if (dt.aGame) {
		string act= "not active";
		if (dt.isActive()) {
			act = "active";
		}
		os << dt.getName() << ": " << dt.toSide(dt.getSide()) << "("<<act<<")" ;
	}
	else {
		os <<  dt.getName() << ": " << dt.getNRubies()<< " rubies";
	}
	return os;
}


