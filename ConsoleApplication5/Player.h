
#include <iostream>
#include <string>
#include "Reward.h"

#ifndef PLAYER_H
#define PLAYER_H
using namespace std;
enum Side { top=0, bottom=1, left=2, right=3 };
class Player
{private:
	
	string name;
	bool active;
	int nRubies;
	bool aGame;
	Side side;
	string toSide(Side tmp) {
		switch (tmp)
		{
		case top:   return "top";
		case bottom:   return "bottom";
		case Side::right: return "right";
		case Side::left:   return "left";
		default:      return "not a side";
		}
	}
public:
	
	Player(string tmp,Side sid):name(tmp), active(true), nRubies(0), aGame(true),side(sid){
	};
	Player():name("None"), active(false), nRubies(0), aGame(false), side(Side::right) {}//constructeur par defaut 
	string getName() const { return name; }
	void setActive(bool Val) { active = Val; }
	bool isActive() { return active; }
	int getNRubies() const{ return nRubies; }
	Side getSide() const;
	void setSide(Side);
	void addReward(const Reward& re){
		nRubies =nRubies+re;
	}
	friend ostream& operator<<(ostream& os, Player& dt);
	void setDisplayMode(bool endOfGame) {
		aGame = endOfGame;
	}
	
};

#endif
