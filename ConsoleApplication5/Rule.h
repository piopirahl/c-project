#pragma once
#include "Game.h"
class Rule
{private:
	int currentPlayer;
	int numPlayer;
public:
	bool isValid(const Game& current) {
		
		if ((current.getCurrentCard())->operator FaceAnimal() == current.getPreviousCard()->operator FaceAnimal()|| 
			(current.getCurrentCard())->operator FaceBackground() == current.getPreviousCard()->operator FaceBackground()) {
			return true;
		}
		else {
			return false;
		}
	}
	bool gameOver(const Game& current) {
		
		if (current.getRound() == 4) {
		
			return true;
		}
		else {
			
			return false;
		}
	}
	bool roundOver(const Game&current) {
		int count = numPlayer;
		for (int i = 0; i < numPlayer; i++) {
			if (!current.getPlayer(static_cast<Side>(i)).isActive()) {
				count--;
			}
		}
		if (count == 1) {
			return true;
		}
		else {
			return false;
		}
	}
	const Player& getNextPlayer(const  Game&);
	Rule(int numPl):currentPlayer(0), numPlayer(numPl){
	
	}
	~Rule();
};

