
// ConsoleApplication5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "board.h"
#include "Game.h"
#include "Rule.h"
#include "RewardDeck.h"
using namespace std;

//TODO: STRING DICTIONARY FOR ERRORS AND OTHER MESSAGES


int conversion(char bob) {
	switch (bob)
	{
	case 'A':   return 0;
	case 'B':   return 1;
	case 'C': return 2;
	case 'D':   return 3;
	case 'E':   return 4;
	case '1':   return 0;
	case '2':   return 1;
	case '3':   return 2;
	case '4':   return 3;
	case '5':   return 4;
	default:    return -1;
	}
}

int main()
{	Board a;
	Board *board=&a;
	Game c(board);
	Game* currentGame = &c;
	RewardDeck* reward;
	reward = &(RewardDeck::make_RewardDeck());
	int numberOfPl = 0;
	cout << "Entrer le nombre de joueur entre 2 et 4:" << endl;
	cin >> numberOfPl;
	while (numberOfPl < 2 || numberOfPl>4) {
		cout << "Enter un nombre valide entre 2 et 4: "; cin >> numberOfPl;
		cout << endl;
	}
	Rule currentRule(numberOfPl);
	vector<string> nameVec;
	for (int i = 0; i < numberOfPl; i++) {
		string tmp;
		cout << "Enter le nom du joueur "<<i+1<<": "; cin >> tmp;
		Player tpl(tmp, static_cast<Side>(i));
		(*currentGame).addPlayer(tpl);
		
	}
	
	cout << c << endl;
	
	while (!currentRule.gameOver(*currentGame)) {
		int rcard = 0;//compteur pour le nombre de carte affiche
		//affiche les cartes devant chaque joueur;
		
		while(!currentRule.roundOver(*currentGame)){
		cout << *board << endl;
		const Player* cuurPlayer = &(currentRule.getNextPlayer(*currentGame));
		int side = static_cast<int>(cuurPlayer->getSide());
		
			cout << "Cartes devant joueur " << (*currentGame).getPlayer(static_cast<Side>(side)).getName() << " sont: " << endl;
			if (side == 0) {
				cout << (*board->getCard(A, Number::two))(1) << " " << (*board->getCard(A, Number::three))(1) << " " << (*board->getCard(A, Number::four))(1) << endl;
				cout << "A2   " << "A3   " << "A4" << endl;
			}
			else if (side == 1) {
				cout << (*board->getCard(E, Number::two))(1) << " " << (*board->getCard(E, Number::three))(1) << " " << (*board->getCard(E, Number::four))(1) << endl;
				cout << "E2   " << "E3   " << "E4" << endl;
			}
			else if (side == 2) {
				cout << (*board->getCard(B, Number::one))(1) << " " << (*board->getCard(C, Number::one))(1) << " " << (*board->getCard(D, Number::one))(1) << endl;
				cout << "B1   " << "C1   " << "D1" << endl;
			}
			else if (side == 3) {
				cout << (*board->getCard(B, Number::five))(1) << " " << (*board->getCard(C, Number::five))(1) << " " << (*board->getCard(D, Number::five))(1) << endl;
				cout << "B5   " << "C5   " << "D5" << endl;
			}

		
		cout << "Joueur " << cuurPlayer->getName() << " choisiser la premiere carte" << endl;
		string tmp; cin >> tmp;
		while (conversion(tmp.at(0)) == -1|| conversion(tmp.at(1)) == -1  ) {
			cout << "Joueur " << cuurPlayer->getName() << " entrer une bonne combinaison" << endl;
		    cin >> tmp;
		}
		Letter row = static_cast<Letter>(conversion(tmp.at(0)));
		Number col = static_cast<Number>(conversion(tmp.at(1)));
		//TODO: MAKE METHOD FOR BELOW LOOP.
		while ((*board).isFaceUp(row, col)) {
			cout << "Joueur " << cuurPlayer->getName() << " entrer une bonne combinaison" << endl;
			cin >> tmp;
			row = static_cast<Letter>(conversion(tmp.at(0)));
			col = static_cast<Number>(conversion(tmp.at(1)));
		}
		
		(*currentGame).setCurrentCard((*currentGame).getCard(row, col));
		(*board).turnFaceUp(row, col);
		cout << "Joueur " << cuurPlayer->getName() << " choisiser la deuxieme carte" << endl;
		cin >> tmp;
		while (conversion(tmp.at(0)) == -1 || conversion(tmp.at(1)) == -1) {
			cout << "Joueur " << cuurPlayer->getName() << " entrer une bonne combinaison" << endl;
			cin >> tmp;
		}
		Letter row2 = static_cast<Letter>(conversion(tmp.at(0)));
		Number col2 = static_cast<Number>(conversion(tmp.at(1)));
		while ((*board).isFaceUp(row, col)) {
			cout << "Joueur " << cuurPlayer->getName() << " entrer une bonne combinaison" << endl;
			cin >> tmp;
			row2 = static_cast<Letter>(conversion(tmp.at(0)));
			col2 = static_cast<Number>(conversion(tmp.at(1)));
		}
		(*currentGame).setCurrentCard((*currentGame).getCard(row2, col2));
		(*board).turnFaceUp(row2, col2);
		cout << *board << endl;
		if (!currentRule.isValid(*currentGame)|| rcard==24) {// Si le joueur choisis une mauvaise combinaison ou qu il ne reste plus de carte a tourner
			(*currentGame).getPlayer(static_cast<Side>(side)).setActive(false);
			if (rcard != 24) {
				(*board).turnFaceDown(row2, col2);
				(*board).turnFaceDown(row, col);
				cout << "Joueur " << cuurPlayer->getName() << " elimine de ce tour" << endl;
			}
		}
		else {
			rcard += 2;
		}
		}
		(*currentGame).setRound();
		(*board).reset();
		for (int i = 0; i < numberOfPl;i++) {
			if ((*currentGame).getPlayer(static_cast<Side>(i)).isActive()) {
				(*currentGame).getPlayer(static_cast<Side>(i)).addReward(*(*reward).getNext());
			}
			(*currentGame).getPlayer(static_cast<Side>(i)).setActive(true);
		}
		
		
		cout << "Round Fini " << endl;
	}cout << "Game Over " << endl;
	int prize = 0;
	vector<Player> winnerDeck;
	for (int i = 0; i < numberOfPl; i++) {
		(*currentGame).getPlayer(static_cast<Side>(i)).setDisplayMode(false);
		winnerDeck.push_back((*currentGame).getPlayer(static_cast<Side>(i)));
	}
	for (int j = 0; j < numberOfPl - 1; j++)//Organise les joueurs en ordre decroissant en fonction de leur rubies
	{
		int iMin = 0;
		for (int i = j + 1; i < numberOfPl; i++)
		{
			if (winnerDeck[i].getNRubies() < winnerDeck[iMin].getNRubies())
			{
				iMin = i;
			}
		}
		if (iMin != j)
		{
			Player tmp = winnerDeck[j];
			winnerDeck[j] = winnerDeck[iMin];
			winnerDeck[iMin] = tmp;
			
		}
	}
	cout << "Score:" << endl;
	for (int i = 0; i < numberOfPl; i++) {
		cout << winnerDeck[i] << endl;
	}
	cout << "Le gagnant est: "<<winnerDeck[numberOfPl-1].getName() << endl;

}

/**
	Board b;
	cout << "-------------------------" << endl;
	cout << b.turnFaceUp(A, three) << endl;

	//cout<<b.isFaceUp(A, one)<<endl;
	cout << "-------------------------" << endl;
	cout << b.turnFaceUp(A, three) << endl;
	cout << "-------------------------" << endl;
	cout << b.turnFaceUp(A, four) << endl;
	cout << "-------------------------" << endl;
	cout << b.turnFaceUp(A, five) << endl;
	cout << "-------------------------" << endl;
	b.setCard(A, five, b.getCard(A, four));
	cout << "-------------------------" << endl;
	cout << b.turnFaceUp(B, five) << endl;
	cout << "-------------------------" << endl;
	cout << b<< endl;**/