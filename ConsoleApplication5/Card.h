#ifndef CARD_H
#define CARD_H
#include <iostream>
#include <string>

using namespace std;
enum FaceBackground { yellow = 0, red = 1, blue = 2, green = 3, purple = 4};
enum FaceAnimal { Crab = 5, Penguin = 6, Octopus = 7, Turle = 8, Walrus = 9};
class Card
{
private:
	int nRows;
	FaceAnimal faceA;
	FaceBackground faceB;
	string cardDeck[4];
	string convertColor(FaceBackground  &obj) {
		switch (obj)
		{
		case yellow:   return "y";
		case red:   return "r";
		case blue: return "b";
		case green:   return "g";
		case purple: return "p";
		default:      return "not a color";
		}

	}
	string convertAnimal(FaceAnimal  &obj) {
		switch (obj)
		{
		case Crab:   return "C";
		case Penguin:   return "P";
		case Octopus: return "O";
		case Turle:   return "T";
		case Walrus: return "W";
		default:      return "not an animal";
		}

	}
	Card(FaceAnimal ani, FaceBackground color):faceA(ani),faceB(color),nRows(4) {
		faceA = ani;
		faceB = color;
		string B = convertColor(faceB);
		string A = convertAnimal(faceA);
		for (int i = 0; i < 4;i++) {
			if (i != 1 && i!=3) {
				
				cardDeck[i] = ""+B+B+B+" ";;
			}
			else if (i==3) {
				cardDeck[i] = "    ";
			}
			else {
				cardDeck[i] = ""+B+A+B+" ";
			}
		}
	};
public:
	
	void operator=(Card const &x) = delete;// Empeche la classe d'etre assigner
	Card(Card const &) = delete;//empeche la classe d'etre copier
	operator FaceBackground() const {
		return faceB;
	}
	friend class CardDeck;
	
	operator FaceAnimal() const {
		return faceA;
	}
	int getNRows() {
		return nRows;
	};
	string operator()(int row) {
		return cardDeck[row];
	}
	
};


#endif
