#ifndef BOARD_H
#define BOARD_H
#include "CardDeck.h"
#include <iostream>
#include <exception>
#include <string>
using namespace std;

const string OUT_OF_RANGE = "Out of Range error: specify a valid number";

enum  Letter{ A = 0, B = 1, C = 2, D = 3,E=4};
enum  Number { one = 0, two = 1, three = 2, four = 3, five=4 };
class Board
{private:
	struct  NoMoreCards : public std::exception//exception lorsqu'un deck est vide;
	{
		const char * what() const throw ()
		{
			return "No more cards available to construct a board";
		}
	};
	Card* arrTable[5][5];
	CardDeck* actualDeck;
	int faceUp[5][5];
	string boardArr[20]{};
	void draw() {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (i == 2 && j == 2) {
					arrTable[i][j] = nullptr;//Carte du milieu
				}
				else {
					arrTable[i][j] = actualDeck->getNext();
					try
					{
						if (arrTable[i][j] == nullptr) {
							throw NoMoreCards();
						}
					}
					catch (NoMoreCards& e)
					{
						std::cout << e.what() << std::endl;
					}
				}
			}
		}		
	}
	void print() {//methode pour construire le tableau de string du Deck
		int current = 0;
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				Card *temp = arrTable[i][j];
				if (faceUp[i][j] && arrTable[i][j] != nullptr) {
					boardArr[current] += (*temp)(0);
					boardArr[current + 1] += (*temp)(1);
					boardArr[current + 2] += (*temp)(2);
					boardArr[current + 3] += (*temp)(3);
				}
				else if (arrTable[i][j] == nullptr) {
					
					boardArr[current] += "    ";
					boardArr[current + 1] += "    ";
					boardArr[current + 2] += "    ";
					boardArr[current + 3] += "    ";
				}
				else {
					string uncovered = "zzz";
					boardArr[current] += uncovered + " ";
					boardArr[current + 1] += uncovered + " ";
					boardArr[current + 2] += uncovered + " ";
					boardArr[current + 3] += "    ";
				}
			}current += 4;
		}
		for (int i = 0; i < 20; i++) {
			string a = " ";
			if (i == 1) {
				a = "A";
			}
			else if (i == 5) {
				a = "B";
				}
			else if (i == 9) {
				a = "C";
			}
			else if (i == 13) {
				a = "D";
			}
			else if (i == 17) {
				a = "E";
			}
			boardArr[i] = a+" "+ boardArr[i];
			cout << boardArr[i] << endl;
			boardArr[i] = "";
	}std::cout << "   1   2   3   4   5 " << endl;
	}//fonction pour dessiner the board
	void validpos(const Letter& let, const Number& num){//fonction pour verifier si la position entrer n est pas celle du milieu
		if (let == C && num == Number::three) {
			throw std::out_of_range("Not a valid position");
		}
	}
public:
	bool isFaceUp(const Letter& let, const Number& num){
		try {
			validpos(let, num);
			return faceUp[static_cast<int>(let)][static_cast<int>(num)];
		}
		catch (const std::out_of_range& oor) {
			std::cerr <<OUT_OF_RANGE <<'\n';
		}
	}
	bool turnFaceUp(const Letter& let, const Number& num) {
		try {
			validpos(let, num);
			if (faceUp[static_cast<int>(let)][static_cast<int>(num)] == true) {

				return false;
			}
			else {
				faceUp[static_cast<int>(let)][static_cast<int>(num)] = true;
				
				return true;

			}

			
			
		}
		catch (const std::out_of_range& oor) { std::cerr <<OUT_OF_RANGE << '\n'; }
	}
	bool turnFaceDown(const Letter& let, const Number& num) {

		try {
			validpos(let, num);
			if (faceUp[static_cast<int>(let)][static_cast<int>(num)] == true) {
				faceUp[static_cast<int>(let)][static_cast<int>(num)] = false;
		
				
				return true;
			}
			else {
				
				return false;

			}

		}
		catch (const std::out_of_range& oor) { std::cerr <<OUT_OF_RANGE << '\n'; }
	}
	Card* getCard(const Letter& let, const Number& num) {
		try {
			validpos(let, num);
			return  arrTable[static_cast<int>(let)][static_cast<int>(num)];
		}
		catch (const std::out_of_range& oor) { std::cerr <<OUT_OF_RANGE << '\n'; }
	}
	void setCard(const Letter&let, const Number&num, Card* test) {
		try {
			validpos(let, num);
			arrTable[static_cast<int>(let)][static_cast<int>(num)] = test;
			print();
		}
		catch (const std::out_of_range& oor) { std::cerr <<OUT_OF_RANGE << '\n'; }
	}
	void reset() {
		for (int i = 0; i < 5; i++) { 
			for(int j=0;j<5;j++)
			faceUp[i][j] = 0; }
		actualDeck->shuffle();
		draw();
	}
	Board() {
		actualDeck = &(CardDeck::make_CardDeck());
		reset();
		
		
	}
	friend ostream& operator<<(ostream& os, Board& dt);
};
#endif

