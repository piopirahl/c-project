#include "Reward.h"
#include "Deck.h"
#ifndef REWARDDECK_H
#define REWARDDECK_H
class RewardDeck :public Deck<Reward>
{protected:
	virtual std::string Name() {
		return "CardDeck";
	}

private:
	RewardDeck():Deck<Reward>(20) {//cree le deck de reward pour les X maches
		for (int i = 0; i < size; i++) {
			vectorDeck.push_back(new Reward());
		}
	}
public:
	void operator=(RewardDeck const &x) = delete;// Empeche la classe d'etre assigner
	RewardDeck(RewardDeck const &) = delete;//empeche la classe d'etre copier
	static RewardDeck& make_RewardDeck();
};
#endif
