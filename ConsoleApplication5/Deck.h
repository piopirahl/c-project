#ifndef DECK_H
#define DECK_H
#include <iostream>
#include <vector> 
#include <algorithm>

using namespace std;
template<typename T>
class Deck
{
protected:
	int size;
	virtual std::string Name() = 0;
	vector<T*> vectorDeck;
	int currentPos;
	Deck() {
		
	}
	Deck(int s) {
		size = s;
		currentPos = 0;
	}
	
	
public:
	void shuffle() {
		currentPos = 0;
		std::random_shuffle(vectorDeck.begin(), vectorDeck.end());
	}
	T* getNext() {
		if (currentPos != size) {
			return vectorDeck.at(currentPos++);
		}
		else {
			return nullptr;
		}
	};
	bool isEmpty() {
		return vectorDeck.empty;
	}
	
};
#endif
