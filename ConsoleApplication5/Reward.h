
#include <random>
#include <iostream>
#ifndef REWARD_H
#define REWARD_H
using namespace std;
class Reward
{
private:
	int prize=0;
	Reward(){
		
		std::default_random_engine rng;
		uniform_int_distribution<int> gen(1, 4); 
		prize= gen(rng);
	}
public:
	void operator=(Reward const &x) = delete;// Empeche la classe d'etre assigner
	Reward(Reward const &) = delete;//empeche la classe d'etre copier
	friend ostream &operator<<(ostream &os, const Reward &item);
	friend class RewardDeck;
	operator int() const {
		return prize;
	}
	
	
};

#endif
